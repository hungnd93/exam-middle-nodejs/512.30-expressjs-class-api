// get all userMiddleware
const getAllUsersMiddleware = (req, res, next) => {
    console.log("Get All User!");
    next();
};
// get a userMiddleware
const getUsersMiddleware = (req, res, next) => {
    console.log("Get a User!");
    next();
};
// Create userMiddleware
const createUsersMiddleware = (req, res, next) => {
    console.log("Create a User!");
    next();
};
// Update userMiddleware
const updateUsersMiddleware = (req, res, next) => {
    console.log("Update a User!");
    next();
};
// Delete userMiddleware
const deleteUsersMiddleware = (req, res, next) => {
    console.log("Delete a User!");
    next();
};
module.exports  = {
    getAllUsersMiddleware,
    getUsersMiddleware,
    createUsersMiddleware,
    updateUsersMiddleware,
    deleteUsersMiddleware
}
