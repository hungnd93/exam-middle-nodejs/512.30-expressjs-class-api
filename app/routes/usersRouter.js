// khai báo thư viện express
const express = require('express');

// Import các middleware
const {
    getAllUsersMiddleware,
    getUsersMiddleware,
    createUsersMiddleware,
    updateUsersMiddleware,
    deleteUsersMiddleware
} = require('../middleware/usersMiddleware');

// Import userList
const {userList} = require('../../data.js');

// Tạo ra Router
const userRouter = express.Router();

// Create new user
userRouter.post('/users', createUsersMiddleware, (req, res) => {
    res.json({
        message: "Create new user successfully!"
    })
});

// get all user
userRouter.get('/users', getAllUsersMiddleware, (req, res) => {
    let age = req.query.age;
    if(age) {
        res.status(200).json({
            result: userList.filter((item) => item.age > age)
        })
    } 
    else {
        res.status(200).json({
            result: userList
        })
    }
});

// get a user by id
userRouter.get('/users/:userId', getUsersMiddleware, (req, res) => {
    let userId = req.params.userId;
    // Do userId nhận được từ params là string nên cần chuyển thành kiểu integer
    userId = Number(userId);
    
    if(userId) {
        res.status(200).json({
            result: userList.filter((item) => item.checkUserById(userId))
        })
    }
    else if(isNaN(userId)){
        res.status(200).json({
            status: "400 Bad Request!"
        })
    }
});

// update a user by id
userRouter.put('/users/:userId', updateUsersMiddleware, (req, res) => {
    res.json({
        message: "Update a user by id successfully!"
    })
});

// delete a user by id
userRouter.delete('/users/:userId', deleteUsersMiddleware, (req, res) => {
    res.json({
        message: "delete a user by successfully!"
    })
});

module.exports = { userRouter }